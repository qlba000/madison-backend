const {Transform} = require('stream');
const chalk = require('chalk');

module.exports = class Tee extends Transform
{
	_transform(chunk, encoding, callback)
	{
		process.stderr.write(chalk.blue([].slice(chunk.toString('hex')).join(' ')), () => callback(null, chunk));
	}
}
