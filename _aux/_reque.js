const {Transform} = require('stream');
const fs = require('fs');

class Reque extends Transform
{
	constructor({file, ...options})
	{
		super(options);

		this.file = fs.createWriteStream(file);
	}

	_transform(chunk, encoding, callback)
	{
		this.file.write(chunk, () => callback(null, chunk));
	}
}

module.exports = (core) =>
{
	const suff = Date.now().toString();

	const ireq = new Reque({file: `./ireq.${suff}`});
	const oreq = new Reque({file: `./oreq.${suff}`});

	ireq.pipe(core.stdin);
	core.stdin = ireq;

	core.stdout.pipe(oreq);
	core.stdout = oreq;
};
