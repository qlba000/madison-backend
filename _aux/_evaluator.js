const _ = require('lodash');
const chalk = require('chalk');
const spawn = require('../utils/spawn');
const IO = require('../utils/io');
const genetic = require('../genetic');
const System = require('../system');
const Tape = require('../utils/tape');
const mfs = require('../classes/mfs');

class Evaluator
{
	async evaluate(paramsVectors)
	{
		this.io.writeUInt(this.maxBlocks);
		this.io.writeUInt(this.maxSamples);

		const tape = new Tape(this.system.getSize());

		for (const vector of paramsVectors)
			for (const {inputs} of this.trainingSet)
			{
				this.system.render(tape, {
					params: _.zipObject(_.keys(this.paramsHash), vector),
					inputs
				});

				console.log(
					_.chunk(tape.getBuffer().toString('hex'), 2).map(x => x.join('')).join(' ')
					
				)

				console.log(tape.getBuffer().length);

				await new Promise((resolve) => this.core.stdin.write(tape.getBuffer(), resolve));

				tape.rewind();
			}

		const zs = [];

		for (let i = 0; i < paramsVectors.length; i++)
		{
			let sum = 0;


			for (const {output} of this.trainingSet)
			{
				const y = await this.io.readFloat();
	
				console.log(y);

				sum += (y - output) ** 2;
			}

			zs.push(Math.sqrt(sum / this.trainingSet.length));
		}

		return zs;
	}

	static async create(params, options)
	{
		const {
			systemDefinition,
			trainingSet,
			mu,
			lambda,

		} = params;
		const {
			corePath,
			coreDeviceNumber,
			coreSpawnOptions,
			maxBlocks,
			maxSamples
		} = options;
		
		const core = spawn(corePath, coreSpawnOptions);
		const io = IO(core.stdout, core.stdin);

		core.stderr.on('data', chunk => process.stdout.write(chalk.red(chunk.toString())));

		io.writeUInt(coreDeviceNumber);

		const Nmax = await io.readULongLong();
		const Gmax = await io.readULongLong();
		const SMPs = await io.readULongLong();

		console.log(`Max samples: ${Nmax}`);
		console.log(`Max global memory: ${Gmax / 1024 ** 3} Gb`);
		console.log(`Streaming multiprocessors: ${SMPs}`);

		const system = new System(systemDefinition);

		io.writeUInt(system.getHeavyCount());
		io.writeUInt(system.getOutCount() * 4);
		io.writeUInt(system.getSize());
		io.writeUInt(maxBlocks); // 28 * 100 // SMPs * 10
		io.writeUInt(maxSamples); // 48 * 1024 / 4 // Nmax

		for (const {inputs} of trainingSet)
			for (const inputName in inputs)
				inputs[inputName] = mfs[inputs[inputName].type].fromDefinition(inputs[inputName]);

		const paramsHash = system.parameters;

		return new Evaluator({system, trainingSet, paramsHash, mu, lambda, core, io, maxBlocks, maxSamples});
	}

	constructor(params)
	{
		Object.assign(this, params);

		await genetic(
			params.mu,
			params.lambda,
			_(params.paramsHash).keys().size(),
			xss => this.evaluate(xss),
			_(params.paramsHash)
				.keys()
				.map(_.propertyOf(params.paramsHash))
				.map(param => ({
					value: param.initial
				})).value(),
			0.15,
			300
		)
	}

	destructor()
	{
		this.io.writeUInt(0);
	}
}

module.exports = Evaluator;
