const _ = require('lodash');
const mfs = require('../classes/mfs');
const ops = require('../classes/ops');
const param = require('../classes/param');

function translate(definition)
{
	const params = {};

	// 1. Process vars
	const mapTerm = (params) => ({name, mf}) => [
		name,
		mfs[mf.type].fromDefinition(mf, params[name] = {})
	];

	const mapVar = (params) => ({name, specs}) => [
		name,
		{
			...specs,
			terms: _.fromPairs(specs.terms.map(mapTerm(params[name] = {})))
		}
	];

	// 1.1. Input vars
	const inputs = _.fromPairs(definition.inputVariables.map(mapVar(params.inputVariables = {})));

	// 1.2. Output vars
	const output = mapVar(params.outputVariables = {})(definition.outputVariables[0])[1];

	if (definition.outputVariables.length !== 1)
		throw new Error('Multiple output systems are not supported yet');


	// 2. Build operations sequence
	const operations = [];

	// 2.1. FTV transformations
	const transformations = {};

	for (const inputName in inputs)
		for (const termName in inputs[inputName].terms)
		{
			_.set(
				transformations,
				[inputName, termName],
				operations.length
			);

			operations.push(new ops.TransformationOp(
				inputs[inputName].terms[termName],
				inputName
			));
		}

	// 2.2. FTV convolutions
	const mapExpression = ({op, args, variable, term}) => op ?
		args.map(mapExpression).reduce(
			(lhs, rhs) => operations.push(new ops.AggregationOp(
				({and: 'conjunction', or: 'disjunction'})[op],
				lhs,
				rhs,
				definition.options.indexNorm.value,
				definition.options.valueNorm.value
			)) - 1
		) :
		transformations[variable][term];

	let rulesTips = _
		.map(definition.rules, 'antecedent')
		.map(mapExpression);

	// 2.3. FTV activations
	definition.rules.forEach(({consequent}, index) =>
		rulesTips[index] = operations.push(new ops.ActivationOp(
			rulesTips[index],
			output.terms[consequent.term],
			output.xMin,
			output.xMax,
			definition.options.model.value,
			definition.options.activNorm.value,
			definition.options.model.value === 'logical' ?
				definition.options.activImpl.value :
				definition.options.activNorm2.value
		)) - 1
	);

	// 2.4. OMF amplification
	definition.rules.forEach(({weight}, index) =>
		rulesTips[index] = operations.push(new ops.AmplificationOp(
			rulesTips[index],
			param(weight, `rules.${index}.weight`, params),
			definition.options.model.value,
			'power'
		)) - 1
	);

	// 2.5. OMF accumulation
	const systemTip = rulesTips.reduce((lhs, rhs) =>
		operations.push(new ops.AccumulationOp(
			lhs,
			rhs,
			definition.options.model.value
		)) - 1
	);

	// 2.6. OMF defuzzification
	operations.push(new ops.DefuzzificationOp(
		systemTip,
		definition.options.defMethod.value,
		output.xMin,
		output.xMax
	))

	// 2.7. Terminator
	operations.push(new ops.ReturnOp());


	// 3. Make constraints
	const parameters = flattenize(params);

	for (const paramName in parameters)
		parameters[paramName].name = paramName;

	const constraints = [];

	// 3.1. Boundaries
	const setVariableBoundaries = variable =>
	{
		const {xMin, xMax} = variable;

		for (const termName in variable.terms)
			variable.terms[termName].setParamsBoundaries(xMin, xMax);
	};

	for (const inputName in inputs)
		setVariableBoundaries(inputs[inputName]);

	setVariableBoundaries(output);


	for (const paramName in parameters)
		if (/^rules\.\d+\.weight$/.test(paramName))
			Object.assign(parameters[paramName], {min: 0, max: 1});


	// 3.2. Centers
	const indices = _(parameters)
		.keys()
		.invert()
		.mapValues(Number)
		.value();

	const processVariable = (variable) =>
	{
		const sortSequence = [];

		for (const termName in variable.terms)
			sortSequence.push(indices[variable.terms[termName].getMiddleParam().name]);

		return sortSequence;
	};

	const sortSequences = [];

	for (const inputName in inputs)
		sortSequences.push(processVariable(inputs[inputName]));

	sortSequences.push(processVariable(output));


	return {
		operations,
		parameters,
		constraints,
		sortSequences,
		trainingSet: definition.trainingSet
	};
}

function flattenize(object, path = [], destination = {})
{
	if (object instanceof param.LearnParam)
		destination[path.join('.')] = object;
	else
		for (const key in object)
			flattenize(object[key], [...path, key], destination);

	return destination;
}

module.exports = translate;
