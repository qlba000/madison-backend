const _ = require('lodash');
const { gauss, uniform, unifint } = require('./random');

const C = 1;

// OBJECTIVE FUNCTION
// X/SIGMA INITIALIZER BY #mu/#n
// NORMALIZER
// STOP CONDITION = {,,}
// ALGORITHM PARAMS (MU, LAMBDA, n)
// LOGGER
module.exports = async function(
	mu,
	lambda,
	n,
	z,
	initRanges,
	sortSequences,
	h,
	rounds = 100,
	logger = defaultLogger
) {
	const tau1 = C / Math.sqrt(2 * n);
	const tau2 = C / Math.sqrt(2 * Math.sqrt(n));

	let population = init(mu, n, initRanges, h), best;

	for (let i = 0; i < rounds; i++)
	{
		population = view(await step(mu, lambda, tau1, tau2, n, z, population, initRanges, sortSequences), i, mu, n, logger);

		if (!best || best.z > population[0].z)
			best = population[0];
	}

	return best.xs;
};

function crossover(n, p1, p2)
{
	const f1 = {}, f2 = {};

	f1.xs = new Array(n);
	f2.xs = new Array(n);
	f1.sigmas = new Array(n);
	f2.sigmas = new Array(n);

	const alpha = uniform(0, 1);

	for (let i = 0; i < n; i++)
	{
		f1.xs[i] = alpha * p1.xs[i] + (1 - alpha) * p2.xs[i];
		f2.xs[i] = alpha * p2.xs[i] + (1 - alpha) * p1.xs[i];
		f1.sigmas[i] = alpha * p1.sigmas[i] + (1 - alpha) * p2.sigmas[i];
		f2.sigmas[i] = alpha * p2.sigmas[i] + (1 - alpha) * p1.sigmas[i];
	}

	return [f1, f2];
}

function mutation(n, tau1, tau2, gene, initRanges, sortSequences)
{
	const result = { xs: gene.xs.slice(), sigmas: gene.sigmas.slice() };

	const N1 = gauss(tau1);

	for (let i = 0; i < n; i++)
		result.sigmas[i] *= Math.exp(N1 + gauss(tau2));

	for (let i = 0; i < n; i++)
		result.xs[i] = _.clamp(
			result.xs[i] + gauss(result.sigmas[i]),
			_.isFinite(initRanges[i].min) ? initRanges[i].min : -Infinity,
			_.isFinite(initRanges[i].max) ? initRanges[i].max : +Infinity
		);

	for (const sequence of sortSequences)
		followOrder(result.xs, sequence);

	return result;
}

async function step(mu, lambda, tau1, tau2, n, z, population, initRanges, sortSequences)
{
	const newPopulation = [];

	for (let i = 0; i < lambda / 2; i++)
	{
		const p1 = population[unifint(0, mu)];
		const p2 = population[unifint(0, mu)];

		const [f1, f2] = crossover(n, p1, p2);

		newPopulation.push(
			mutation(n, tau1, tau2, f1, initRanges, sortSequences),
			mutation(n, tau1, tau2, f2, initRanges, sortSequences)
		);
	}

	(await z(newPopulation.map(g => g.xs))).forEach((z, i) => newPopulation[i].z = z);

	return [
		...newPopulation,
		// ...population.slice(0, 2)
	].sort((a, b) => a.z - b.z).slice(0, mu);
}

function init(mu, n, initRanges, h)
{
	const population = [];

	for (let i = 0; i < mu; i++)
	{
		const xs = [], sigmas = [];

		for (let j = 0; j < n; j++) 
		{
			const {max, min, value, sigma} = initRanges[j];

			xs.push(isFinite(value) ? value : uniform(min, max));
			sigmas.push(sigma || h * (max - min) || 1);
		}

		population.push({ xs, sigmas });
	}

	return population;
}

function followOrder(xs, order)
{
	const pick = order
		.map(index => xs[index])
		.sort((a, b) => a - b);

	for (let i = 0; i < order.length; i++)
		xs[order[i]] = pick[i];
}


const sprintf = require('printf');

function view(population, round, mu, n, logger)
{
	const best = population[0].z;
	const aver = population
		.map(({z}) => z).reduce((acc, val) => acc + val, 0) / population.length;
	const bdis = population[0].sigmas
		.reduce((acc, val) => acc + val, 0) / n;
	const disp = population.reduce((acc, {sigmas}) =>
		acc + sigmas.reduce((acc, val) => acc + val, 0),
	0) / mu / n;

	logger({round, best, aver, bdis, disp});

	return population;
}

function defaultLogger({round, best, aver, bdis, disp})
{
	process.stdout.write(sprintf('%10d %15.10f %15.10f %15.10f %15.10f\n',
		round, best, aver, bdis, disp));
}
