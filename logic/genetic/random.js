const N = 12;

function gauss()
{
	let sum = 0;

	for (let i = 0; i < N; i++)
		sum += Math.random();

	return (sum - N / 2) * Math.sqrt(12 / N);
}

module.exports.uniform = (a, b) => a + (b - a) * Math.random();
module.exports.unifint = (a, b) => a + Math.floor(((b - a) * Math.random()) % (b - a));
module.exports.gauss = (sigma) => gauss(N) * sigma;
