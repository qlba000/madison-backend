const _ = require('lodash');
const chalk = require('chalk');
const spawn = require('../utils/spawn');
const IO = require('../utils/io');
const System = require('./system');
const genetic = require('./genetic');
const Tape = require('../utils/tape');
const mfs = require('../classes/mfs');

module.exports = async (systemDefinition, params, options, messageHandler) =>
{
	const system = new System(systemDefinition);
	const {mu, lambda, rounds} = params;
	const {
		corePath,
		coreDeviceNumber,
		coreSpawnOptions,
		maxBlocks,
		maxSamples
	} = options;
	
	const core = spawn(corePath, coreSpawnOptions);
	const io = IO(core.stdout, core.stdin);

	core.stderr.on('data', process.env.MADISON_CORE_PIPE_STDERR ?
		chunk => log(chalk.red(chunk.toString())) : _.noop);

	io.writeUInt(coreDeviceNumber);

	const Nmax = await io.readULongLong();
	const Gmax = await io.readULongLong();
	const SMPs = await io.readULongLong();

	if (process.env.MADISON_CORE_PIPE_STDERR)
	{
		console.log(`Max samples: ${Nmax}`);
		console.log(`Max global memory: ${Gmax / 1024 ** 3} Gb`);
		console.log(`Streaming multiprocessors: ${SMPs}`);
	}

	io.writeUInt(system.getHeavyCount());
	io.writeUInt(system.getOutCount() * 4);
	io.writeUInt(system.getSize());
	io.writeUInt(maxBlocks); // 28 * 100 // SMPs * 10
	io.writeUInt(maxSamples); // 48 * 1024 / 4 // Nmax

	for (const {inputs} of system.trainingSet)
		for (const inputName in inputs)
			inputs[inputName] = mfs[inputs[inputName].type].fromDefinition(inputs[inputName]);

	const paramsHash = system.parameters;


	const evaluate = async (paramsVectors, returnOutput) =>
	{
		const tape = new Tape(system.getSize());

		let sysCount = 0, sysLeft = paramsVectors.length * system.trainingSet.length;

		for (const vector of paramsVectors)
			for (const {inputs} of system.trainingSet)
			{
				if (sysCount % maxBlocks === 0)
				{
					io.writeUInt(Math.min(maxBlocks, sysLeft));
					io.writeUInt(maxSamples);
				}

				system.render(tape, {
					params: _.zipObject(_.keys(paramsHash), vector),
					inputs
				});

				await new Promise((resolve) => core.stdin.write(tape.getBuffer(), resolve));

				tape.rewind();

				sysCount++;
				sysLeft--;
			}

		const zs = [], outputs = [];

		for (let i = 0; i < paramsVectors.length; i++)
		{
			let sum = 0;


			for (const {output} of system.trainingSet)
			{
				const y = await io.readFloat();

				outputs.push(y);

				sum += (y - output) ** 2;
			}

			zs.push(Math.sqrt(sum / system.trainingSet.length));
		}

		return returnOutput ? outputs : zs;
	};


	const result = params.paramsVectors ?
		await evaluate(params.paramsVectors, true) :
		await genetic(
			mu,
			lambda,
			_(paramsHash).keys().size(),
			evaluate,
			_(paramsHash)
				.keys()
				.map(_.propertyOf(paramsHash))
				.map(param => ({
					min: param.min,
					max: param.max,
					value: param.initial
				})).value(),
			system.sortSequences,
			0.15,
			rounds,
			messageHandler
		);

	io.writeUInt(0);

	messageHandler({result: {params: _.keys(paramsHash), values: result}});

	return result;
};

function log(...argv)
{
	if (process.env.MADISON_CORE_PIPE_STDERR)
		process.stdout.write(...argv);
}
