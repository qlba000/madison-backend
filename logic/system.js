const _ = require('lodash');
const translator = require('./translator');

class System
{
	constructor(definition)
	{
		_.extend(this, translator(definition));
	}

	render(tape, ctx)
	{
		let op = 0;

		for (const operation of this.operations)
		{
			const text = operation.render(tape, ctx);

			// console.log(String(op++).padStart(3), text);
		}
	}

	getHeavyCount()
	{
		return _(this.operations).map('isHeavy').map(Number).sum();
	}

	getOutCount()
	{
		return _(this.operations).map('isOut').map(Number).sum();
	}

	getSize()
	{
		return _(this.operations).map('size').sum();
	}
}

module.exports = System;
