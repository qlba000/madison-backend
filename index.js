const executor = require('./logic/executor');

async function main({systemDefinition, params, options})
{
	process.on('message', (message) =>
	{
		if (message.stop)
			process.exit(-1);
	});

	await executor(
		systemDefinition,
		{
			mu: 250, // ipc
			lambda: 250 * 4, // ipc
			rounds: 300,
			...params
		},
		{
			corePath: process.env.MADISON_CORE_PATH, // config, env, ipc
			coreDeviceNumber: process.env.MADISON_DEVICE_NUMBER || 0, // config, env, ipc
			coreSpawnOptions: {env: {OMP_NUM_THREADS: 1}}, // config, env, ipc
			maxBlocks: 28 * 40, // config, env
			maxSamples: 48 * 1024 / 4, // config, env, ipc
			...options
		},
		message => process.send(message)
	);

	process.removeAllListeners();
}

function onError(error)
{
	console.error(error);
	process.send({error}, () => process.exit(-1));
}

process.on('uncaughtException', onError);
process.on('unhandledRejection', onError);

process.once('message', message => main(message.task));



// main().then(x => console.dir(x));
