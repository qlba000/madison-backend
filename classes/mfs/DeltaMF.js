const enums = require('../enums');
const param = require('../param');

class DeltaMF
{
	render(tape, ctx)
	{
		tape.int32(enums.membership.delta);
		tape.float(this.x.get(ctx));

		return ['DELTA', this.x.get(ctx)].join(' ');
	}

	setParamsBoundaries(min, max)
	{
		Object.assign(this.x, {min, max});
	}

	getMiddleParam()
	{
		return this.x;
	}

	constructor(x)
	{
		Object.assign(this, {x});
	}

	static fromDefinition({deltaX}, outParams)
	{
		return new DeltaMF(param(deltaX, 'deltaX', outParams));
	}
}

module.exports = DeltaMF;
