const enums = require('../enums');
const param = require('../param');

class GaussMF
{
	render(tape, ctx)
	{
		tape.int32(enums.membership.gauss);
		tape.float(this.m.get(ctx));
		tape.float(this.s.get(ctx));

		return ['GAUSS', this.m.get(ctx), this.s.get(ctx)].join(' ');
	}

	setParamsBoundaries(min, max)
	{
		Object.assign(this.m, {min, max});
		Object.assign(this.s, {min: 0});
	}
	
	getMiddleParam()
	{
		return this.m;
	}

	constructor(m, s)
	{
		Object.assign(this, {m, s});
	}

	static fromDefinition({gaussMu, gaussSigma}, outParams)
	{
		return new GaussMF(
			param(gaussMu, 'gaussMu', outParams),
			param(gaussSigma, 'gaussSigma', outParams)
		);
	}
}

module.exports = GaussMF;
