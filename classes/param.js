class ConstParam
{
	constructor(value)
	{
		this.value = value;
	}

	get()
	{
		return this.value;
	}
}

class LearnParam
{
	constructor(initial)
	{
		this.initial = this.value = initial;
	}

	getInitial()
	{
		return this.initial;
	}

	get(ctx)
	{
		return ctx.params[this.name]; // this.value
	}

	set(value)
	{
		this.value = value;
	}
}

module.exports = ({value, learn}, name, outParams) =>
{
	if (learn === true)
		return outParams[name] = new LearnParam(value);
	else
		return new ConstParam(value);
};

module.exports.LearnParam = LearnParam;
