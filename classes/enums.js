exports.model = {
	logical: 0x000,
	mamdani: 0x001
};

exports.norm = {
	maxmin: 0x100,
	probabilistic: 0x101,
	lukasiewicz: 0x102,
	strong: 0x103,
	nullpotent: 0x104
};

exports.implication = {
	standard: 0x200,
	arithmetical: 0x201,
	lukasiewicz: 0x202,
	aliev1: 0x203,
	aliev2: 0x204,
	aliev4: 0x205,
	godel: 0x206,
	richebach: 0x207,
	zadeh: 0x208,
	recher: 0x209,
	clincidenis: 0x210,
	hoguen: 0x211,
	fouett: 0x212,
	vadhi: 0x213
};

exports.membership = {
	delta: 0x300,
	gauss: 0x301
};

exports.aggregation = {
	and: 0x400,
	or: 0x401
};

exports.amplification = {
	power: 0x500,
	press: 0x501,
	shift: 0x502,
	drown: 0x503
};

exports.defuzzification = {
	cog: 0x600,
	coa: 0x601
};

exports.op = {
	return: 0x700,
	ftv: {
		transformation: 0x701,
		aggregation: {
			conjunction: 0x702,
			disjunction: 0x703
		},
		negation: 0x704,
		activation: 0x705
	},
	omf: {
		amplification: 0x706,
		accumulation: 0x707,
		defuzzification: 0x708
	}
};
