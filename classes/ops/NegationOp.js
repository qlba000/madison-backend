const enums = require('../enums');

module.exports = class NegationOp
{
	constructor(A)
	{
		Object.assign(this, {
			A,
			isHeavy: true,
			isOut: false,
			size: 2 * 4
		});
	}

	render(tape)
	{
		tape.int32(enums.op.ftv.negation);
		tape.int32(this.A);

		return ['NEG', this.A].join(' ');
	}
};
