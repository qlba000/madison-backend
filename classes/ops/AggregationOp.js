const enums = require('../enums');

module.exports = class AggregationOp
{
	constructor(type, A, B, indexNorm, valueNorm)
	{
		Object.assign(this, {
			type,
			A,
			B,
			indexNorm,
			valueNorm,
			isHeavy: true,
			isOut: false,
			size: 5 * 4
		});

		if (type !== 'conjunction' && type !== 'disjunction')
			throw new Error();
	}

	render(tape)
	{
		tape.int32(enums.op.ftv.aggregation[this.type]);
		tape.int32(this.A);
		tape.int32(this.B);
		tape.int32(enums.norm[this.indexNorm]);
		tape.int32(enums.norm[this.valueNorm]);

		return ['AGG', this.type, this.A, this.B, this.indexNorm, this.valueNorm].join(' ');
	}
};
