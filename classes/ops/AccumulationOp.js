const enums = require('../enums');

module.exports = class AccumulationOp
{
	constructor(A, B, model)
	{
		Object.assign(this, {
			A,
			B,
			model,
			isHeavy: true,
			isOut: false,
			size: 4 * 4
		});
	}

	render(tape)
	{
		tape.int32(enums.op.omf.accumulation);
		tape.int32(this.A);
		tape.int32(this.B);
		tape.int32(enums.model[this.model]);

		return ['ACC', this.A, this.B, this.model].join(' ');
	}
};
