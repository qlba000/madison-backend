const enums = require('../enums');

module.exports = class ActivationOp
{
	constructor(A, BMF, a, b, model, norm, implication)
	{
		Object.assign(this, {
			A,
			BMF,
			a,
			b,
			model,
			norm,
			implication,
			isHeavy: true,
			isOut: false,
			size: (7 + 3) * 4
		});
	}

	render(tape, ctx)
	{
		tape.int32(enums.op.ftv.activation);
		tape.int32(this.A);
		const bmf = this.BMF.render(tape, ctx);
		tape.float(this.a);
		tape.float(this.b);
		tape.int32(enums.model[this.model]);
		tape.int32(enums.norm[this.norm]);
		tape.int32(enums[this.model === 'mamdani' ? 'norm' : 'implication'][this.implication]);

		return ['ACT', this.A, `(${bmf})`, this.a, this.b, this.model, this.norm, this.implication].join(' ');
	}
};
