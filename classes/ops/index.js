exports.TransformationOp = require('./TransformationOp');
exports.AggregationOp = require('./AggregationOp');
exports.ActivationOp = require('./ActivationOp');
exports.AmplificationOp = require('./AmplificationOp');
exports.AccumulationOp = require('./AccumulationOp');
exports.DefuzzificationOp = require('./DefuzzificationOp');
exports.ReturnOp = require('./ReturnOp');
