const enums = require('../enums');

module.exports = class DefuzzificationOp
{
	constructor(A, method, a, b)
	{
		Object.assign(this, {
			A,
			method,
			a,
			b,
			isHeavy: false,
			isOut: true,
			size: 5 * 4
		});
	}

	render(tape)
	{
		tape.int32(enums.op.omf.defuzzification);
		tape.int32(this.A);
		tape.int32(enums.defuzzification[this.method]);
		tape.float(this.a);
		tape.float(this.b);

		return ['DFZ', this.A, this.method, this.a, this.b].join(' ');
	}
};
