const enums = require('../enums');

module.exports = class AmplificationOp
{
	constructor(A, w, model, method)
	{
		Object.assign(this, {
			A,
			w,
			model,
			method,
			isHeavy: true,
			isOut: false,
			size: 5 * 4
		});
	}

	render(tape, ctx)
	{
		tape.int32(enums.op.omf.amplification);
		tape.int32(this.A);
		tape.float(this.w.get(ctx));
		tape.int32(enums.model[this.model]);
		tape.int32(enums.amplification[this.method]);

		return ['AMP', this.A, this.w.get(ctx), this.model, this.method].join(' ');
	}
};
