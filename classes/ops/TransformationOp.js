const enums = require('../enums');

module.exports = class TransformationOp
{
	constructor(premiseMF, factInputName)
	{
		Object.assign(this, {
			premiseMF,
			factInputName,
			isHeavy: true,
			isOut: false,
			size: (1 + 2 * 3) * 4
		});
	}

	render(tape, ctx)
	{
		const fact = ctx.inputs[this.factInputName];

		tape.int32(enums.op.ftv.transformation);
		const prem = this.premiseMF.render(tape, ctx);
		const fakt = fact.render(tape, ctx);

		return ['FTV', `(${prem})`, `(${fakt})`].join(' ');
	}
};
