const enums = require('../enums');

module.exports = class ReturnOp
{
	constructor()
	{
		Object.assign(this, {
			isHeavy: false,
			isOut: false,
			size: 1 * 4
		});
	}

	render(tape)
	{
		tape.int32(enums.op.return);

		return ['RET'].join(' ');
	}
};
