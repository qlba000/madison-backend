const child_process = require('child_process');

module.exports = (command, options) =>
{
	const subprocess = child_process.spawn(command, [], options);

	subprocess.on('close', (code, signal) =>
	{
		log(`subprocess closed with code ${code} and signal ${signal}`);
	});

	subprocess.on('disconnect', () =>
	{
		log('subprocess disconnected');
	});

	subprocess.on('error', (err) =>
	{
		log(`subprocess error: ${err.message}`);
	});

	subprocess.on('exit', (code, signal) =>
	{
		log(`subprocess exited with code ${code} and signal ${signal}`);
	});

	subprocess.on('message', (message) =>
	{
		log(`subprocess sent message ${message}`);
	});

	return subprocess;
};

function log(...argv)
{
	if (process.env.MADISON_CORE_PIPE_STDERR)
		console.log(...argv);
}
