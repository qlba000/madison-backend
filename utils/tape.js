module.exports = class Tape
{
	constructor(length)
	{
		this.buffer = Buffer.alloc(length);
		this.rewind();
	}

	rewind()
	{
		this.buffer.fill(0);
		this.offset = 0;
	}

	getBuffer()
	{
		return this.buffer;
	}

	getLength()
	{
		return this.offset;
	}

	int32(value)
	{
		if (!Number.isSafeInteger(value))
			throw new Error('Value is not a safe integer');

		this.offset = this.buffer.writeInt32LE(value, this.offset);
	}

	float(value)
	{
		if (!Number.isFinite(value))
			throw new Error('Value is not finite');

		this.offset = this.buffer.writeFloatLE(value, this.offset);
	}
};
