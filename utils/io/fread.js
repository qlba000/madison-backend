const {EventEmitter} = require('events');

module.exports = class extends EventEmitter
{
	constructor(stream)
	{
		super();

		this.buf = Buffer.allocUnsafe(0);

		stream.on('data', data =>
		{
			this.buf = Buffer.concat([this.buf, data]);
			this.emit('data');
		});
	}

	read(length)
	{
		return new Promise(resolve =>
		{
			this.on('data', () =>
			{
				if (this.buf.length >= length)
				{
					this.removeAllListeners();

					const slice = this.buf.slice(0, length);
					this.buf = this.buf.slice(length);

					resolve(slice);
				}
			});

			this.emit('data');
		});
	}
}
