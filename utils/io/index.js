const Fread = require('./fread');

module.exports = (istream, ostream) =>
{
	const reader = new Fread(istream), io = {};

	io.writeUInt = (int) =>
	{
		const buf = Buffer.allocUnsafe(4);

		buf.writeUInt32LE(int, 0);

		ostream.write(buf);
	};

	io.readFloat = async () =>
	{
		const buf = await reader.read(4);

		return buf.readFloatLE(0);
	}

	io.readULongLong = async () =>
	{
		const buf = await reader.read(8);

		const lo = buf.readUInt32LE(0);
		const hi = buf.readUInt32LE(4);

		return (hi * (2 ** 32)) + lo;
	};

	return io;
}
